# Tracking Algorithms for PDR

These are the tracking algorithms implemented in the Preliminary Design Review.
Specifically, they are the  quaternion algorithms with the MATLAB code to generate a simulated tracking scenario with the UAV and UGV.
