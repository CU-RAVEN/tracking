function [] = plot_Euler(E,n,n_total)
% Drone camera 3-2-1
subplot(2,1,1); hold on
grid on ; ax = gca; ax.GridLineStyle = '--';
xlim([0,n_total]); ylim([-180,180])
xlabel('n'); ylabel('Drone 3-2-1 [deg]')
plot(n,rad2deg(E.dc(1)),'r.','markersize',10) %psi
plot(n,rad2deg(E.dc(2)),'g.','markersize',10) %theta
plot(n,rad2deg(E.dc(3)),'b.','markersize',10) %phi
legend('\psi','\theta','\phi')

% Rover camera 3-2-1
subplot(2,1,2); hold on
grid on ; ax = gca; ax.GridLineStyle = '--';
xlim([0,n_total]); ylim([-180,180])
xlabel('n'); ylabel('Rover 3-2-1 [deg]')
plot(n,rad2deg(E.rc(1)),'r.','markersize',10) %psi
plot(n,rad2deg(E.rc(2)),'g.','markersize',10) %theta
plot(n,rad2deg(E.rc(3)),'b.','markersize',10) %phi
legend('\psi','\theta','\phi')
end

