function [E,cam,h] = plot_frames(r,R,vec_length)
%% Initialize variables 
X = [vec_length 0 0];
Y = [0 vec_length 0];
Z = [0 0 vec_length];

% %% Vehicles
% h.d = scatter3(r.d(1),r.d(2),r.d(3),150,'x','linewidth',1.25,'markeredgecolor',[.4 .4 .4]);
% h.r = scatter3(r.r(1),r.r(2),r.r(3),70,'filled','ys','linewidth',1.25,'markeredgecolor','k');

%% Inertial frame
quiver3(0, 0, 0, max(xlim), 0, 0, 'r', 'linestyle', ':', ... %x-axis
    'linewidth',1,'showarrowhead','off');
quiver3(0, 0, 0, 0, max(ylim), 0, 'g', 'linestyle', ':', ... %y-axis
    'linewidth',1,'showarrowhead','off');
quiver3(0, 0, 0, 0, 0, max(zlim), 'b', 'linestyle', ':', ... %z-axis
    'linewidth',1,'showarrowhead','off');

%% Relative vector
h.rel = quiver3(r.r(1), r.r(2), r.r(3), r.rel(1), r.rel(2), r.rel(3), ...
    'k','linestyle', ':', 'linewidth',0.5,'showarrowhead','off');

%% Drone
% Rotate vectors
x.d = R.d*X';
y.d = R.d*Y';
z.d = R.d*Z';

h.dx = quiver3(r.d(1), r.d(2), r.d(3), x.d(1), y.d(1), z.d(1),... %x-axis
    'r','linewidth',1,'maxheadsize',0.5);
h.dy = quiver3(r.d(1), r.d(2), r.d(3), x.d(2), y.d(2), z.d(2),... %y-axis
    'g','linewidth',1,'maxheadsize',0.5);
h.dz = quiver3(r.d(1), r.d(2), r.d(3), x.d(3), y.d(3), z.d(3),... %z-axis
    'b','linewidth',1,'maxheadsize',0.5);

%% Rover
% Rotate vectors
x.r = R.r*X';
y.r = R.r*Y';
z.r = R.r*Z';

h.rx = quiver3(r.r(1), r.r(2), r.r(3), x.r(1), y.r(1), z.r(1),... %x-axis
    'r','linewidth',1,'maxheadsize',0.5);
h.ry = quiver3(r.r(1), r.r(2), r.r(3), x.r(2), y.r(2), z.r(2),... %y-axis
    'g','linewidth',1,'maxheadsize',0.5);
h.rz = quiver3(r.r(1), r.r(2), r.r(3), x.r(3), y.r(3), z.r(3),... %z-axis
    'b','linewidth',1,'maxheadsize',0.5);

%% Drone camera
num = dot(r.rel,[x.d(1), y.d(1), z.d(1)]);
den = norm(r.rel)*norm([x.d(1), y.d(1), z.d(1)]);
theta.dc = pi-acos(num/den);
v = cross(r.rel,[x.d(1), y.d(1), z.d(1)])/norm(cross(r.rel,[x.d(1), y.d(1), z.d(1)]));
[Q,E.dc] = quaternion(v,theta.dc);
x.dc = Q*[x.d(1), y.d(1), z.d(1)]';

% quiver3(r.d(1), r.d(2), r.d(3), v(1), v(2), v(3),...
%     'c','linewidth',1,'maxheadsize',0.5);
h.dc = quiver3(r.d(1), r.d(2), r.d(3), x.dc(1), x.dc(2), x.dc(3),...
    'k','linewidth',1,'maxheadsize',0.5);

%% Rover camera
num = dot(r.rel,[x.r(1), y.r(1), z.r(1)]);
den = norm(r.rel)*norm([x.r(1), y.r(1), z.r(1)]);
theta.rc = 2*pi-acos(num/den);
v = cross(r.rel,[x.r(1), y.r(1), z.r(1)])/norm(cross(r.rel,[x.r(1), y.r(1), z.r(1)]));
[Q,E.rc] = quaternion(v,theta.rc);
x.rc = Q*[x.r(1), y.r(1), z.r(1)]';

% quiver3(r.r(1), r.r(2), r.r(3), v(1), v(2), v(3),...
%     'c','linewidth',1,'maxheadsize',0.5);
h.rc = quiver3(r.r(1), r.r(2), r.r(3), x.rc(1), x.rc(2), x.rc(3),...
    'k','linewidth',1,'maxheadsize',0.5);

%% Store camera turning angles
cam.d = theta.dc;
cam.r = theta.rc;

end

