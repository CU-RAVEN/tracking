function [E] = proj_quat(a,b)
% Roll angle
num = dot(a(2:3),b(2:3));
den = norm(a(2:3))*norm(b(2:3));
roll = acos(num/den);

% Pitch angle
num = dot([a(1) a(3)],[b(1) b(3)]);
den = norm([a(1) a(3)])*norm([b(1) b(3)]);
pitch = acos(num/den);

% Yaw  angle
num = dot(a(1:2),b(1:2));
den = norm(a(1:2))*norm(b(1:2));
yaw = acos(num/den);

E = [roll, pitch, yaw];
end

