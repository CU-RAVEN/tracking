function [Q,E] = quaternion(v,theta)
% Create quaternion (first term is scalar, rest is unit vector)
q = [cos(theta/2), sin(theta/2)*v];

% Rename terms for DCM
a = q(1); b = q(2); c = q(3); d = q(4);
Q = [1-2*c^2-2*d^2    2*b*c-2*a*d       2*b*d+2*a*c;
     2*b*c+2*a*d      1-2*b^2-2*d^2     2*c*d-2*a*b;
     2*b*d-2*a*c      2*c*d+2*a*b       1-2*b^2-2*c^2];
 
% Convert to Euler angles (321 rotation)
E = [atan2(2*(a*d+b*c),1-2*(c^2+d^2));      %psi (x)
     asin(2*(a*c-d*b));                     %theta (y)
     atan2(2*(a*b+c*d),1-2*(b^2+c^2))];     %phi (z)
end