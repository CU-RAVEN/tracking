function [] = plot_theta(cam,cam_prev,n,n_total)
% Theta vs time
subplot(2,1,1); hold on
grid on ; ax = gca; ax.GridLineStyle = '--';
xlim([0,n_total]); ylim([0,360])
xlabel('n'); ylabel('$\theta$ [deg]')
plot(n,rad2deg(cam.d),'r.','markersize',10) %drone
plot(n,rad2deg(cam.r),'b.','markersize',10) %rover
% if time == length-1
    legend('UAV','UGV')
% end

% Theta rate vs time
subplot(2,1,2); hold on
grid on ; ax = gca; ax.GridLineStyle = '--';
xlim([0,n_total]); ylim([-5,5])
xlabel('n'); ylabel('$\dot{\theta}$ [deg/n]')
plot(n,rad2deg(cam.d-cam_prev.d)./n,'r.','markersize',10) %drone
plot(n,rad2deg(cam.r-cam_prev.r)/n,'b.','markersize',10)  %rover
% if time == length-1
    legend('UAV','UGV')
% end
end

