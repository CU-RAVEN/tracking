%==========================================================================
% Author:       Rolf A.
% Created:      10.5.17
% Edited:       10.9.17
% Description:  This program represents the pointing algorithm for the
% drone-rover pair's vision system. It uses quaternions to adjust attitude.
%==========================================================================
clear all; close all; clc; format longG
set(0,'DefaultTextInterpreter','latex'); set(0,'DefaultAxesFontsize',11);

%% Initialize Simulation Values

d = 30;                                 %initial distance between vehicles [m]
phi = pi/4;                             %inertial pitch angle [rad]
r.r = [0 0 0];                          %initial rover position vector [m]
roll.d = 0; pitch.d = 0;                %drone attitude [rad]
roll.r = 0; pitch.r = 0; yaw.r = -pi/4; %rover attitude [rad]

% Choose simulation scheme
fprintf('---------------------------------------\n')
fprintf('Choose #:\n1 - drone circ. orbit, rover stationary\n2 - drone hover, rover translate\n3 - hybrid\n')
fprintf('---------------------------------------\n')
sim = input('Choice: ');
switch sim
    case 1
        psi = deg2rad(0:5:360); %inertial yaw angles for circular orbit [rad]
        vel.r = 0;              %rover velocity magnitude [m/n]
    case 2
        psi = zeros(1,360/5+1); %inertial yaw angles for hover [rad]
        vel.r = .5;             %rover velocity magnitude [m/n]
    case 3
        psi = deg2rad(0:5:360); %inertial yaw angles for circular orbit [rad]
        vel.r = .5;             %rover velocity magnitude [m/n]
end
omega.d = psi(end)/numel(psi);  %[rad/n]
vel.d = d*cos(phi)*omega.d;     %[m/n]

%% Run Simulation

figure; hold on
xlabel('X [m]'); ylabel('Y [m]'); zlabel('Z [m]')
xlim([-40 40]); ylim([-40 40]); zlim([0 40]);
vec_length = max(zlim)/5;
view(120,25); grid on 
ax = gca; ax.GridLineStyle = '--';

for n = 1:length(psi)
    % Update position and attitude vectors
    r.d = [d*cos(psi(n)) d*sin(psi(n)) d*sin(phi)];
    r.r = r.r+[vel.r*sin(abs(yaw.r)),vel.r*cos(abs(yaw.r)),0];
    yaw.d = -pi/2-psi(n); %for circular drone orbit

    % Plot vehicle/camera coordinate frames
    r.rel = r.d-r.r;
    R.d = rotation(roll.d,pitch.d,yaw.d); 
    R.r = rotation(roll.r,pitch.r,yaw.r);
    [E{n}, cam{n}, h] = plot_frames(r,R,vec_length);
    tit = ['n = ' num2str(n)];
    title({['$V_d$ = ' num2str(vel.d) ' m/n, $\theta_d$ = ' num2str(rad2deg(cam{n}.d))...
        '$^{\circ}$'],['$V_r$ = ' num2str(vel.r) ' m/n, $\theta_r$ = '... 
        num2str(rad2deg(cam{n}.r)) '$^{\circ}$'], ['$\indent\indent$' tit]})
%     if n > 36
%         view(new_view-n/2,25)
%     else
%         view(120+n/2,25)
%         new_view = 120+n;
%     end
    drawnow limitrate
    F(n) = getframe(gcf);
        
    % Delete previous handles (less clutter)
    if n~=length(psi)
        %delete(h.d); delete(h.r); 
        delete(h.rel)
        delete(h.dx); delete(h.dy); delete(h.dz);
        delete(h.rx); delete(h.ry); delete(h.rz);
        delete(h.dc); delete(h.rc)
    end
end
video = VideoWriter('Pointing_Sim','MPEG-4');
video.Quality = 100;
video.FrameRate = 7;
open(video)
writeVideo(video,F)
close(video)


%% Plot 3-2-1 Rotation Sequence

% figure
% for n = 1:numel(psi)
%     plot_Euler(E{n},n,numel(psi))
%     drawnow limitrate
% end

% Camera angle (quat)
% figure
% for n = 1:numel(psi)-1
%     plot_theta(cam{n+1},cam{n},n,numel(psi))
%     drawnow limitrate
% end
